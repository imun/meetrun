﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MeetRun.Core.Domain.Types;
using MeetRun.Web.ViewModels.Base;

namespace MeetRun.Web.ViewModels.Entities {
    public class CompanyViewModel: BaseEntityViewModel {
        #region Main Properties
        public long Id { get; set; }
        public string Title { get; set; }
        public string EnTitle { get; set; }
        public string EconomyId { get; set; }
        public string RegisterNumber { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string CeoName { get; set; }
        public string CeoPhone { get; set; }
        public string LogoPath { get; set; }
        public bool IsPersonal { get; set; }
        public EntityStatus Status { get; set; }
        #endregion
    }
}
