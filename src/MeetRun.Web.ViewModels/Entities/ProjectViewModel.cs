﻿using MeetRun.Web.ViewModels.Base;
using MeetRun.Core.Domain.Types;
using System.ComponentModel.DataAnnotations;
using MeetRun.Web.Resources;
using System.Collections.Generic;

namespace MeetRun.Web.ViewModels.Entities {
    public class ProjectViewModel: BaseEntityViewModel {
        #region Properties
        public long Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_ProjectName")]
        [StringLength(500, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Title { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Description")]
        [StringLength(2000, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Description { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Description")]
        [StringLength(2000, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Code { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Description")]
        [StringLength(2000, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string LogoPath { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Status")]
        public ProjectStatus Status { get; set; }

        public long UserId { get; set; }
        public long CompanyId { get; set; }
        #endregion

        #region Collections
        public DropDownViewModel StatusList =>
            new DropDownViewModel {
                items = new List<DropDownItemViewModel>() {
                    new DropDownItemViewModel { id = 0 , display = "غیرفعال" },
                    new DropDownItemViewModel { id = 1 , display = "فعال" },
                    new DropDownItemViewModel { id = 2 , display = "شروع شده" },
                    new DropDownItemViewModel { id = 3 , display = "درحال پیشرفت" },
                    new DropDownItemViewModel { id = 4 , display = "پایان یافته" },
                    new DropDownItemViewModel { id = 5 , display = "شکست خورده" }
                }
            };

        #endregion
    }
}
