﻿using MeetRun.Core.Domain.Types;
using MeetRun.Web.ViewModels.Base;

namespace MeetRun.Web.ViewModels.Entities {
    public class UserViewModel: BaseEntityViewModel {
        #region Main Properties
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string TelegramId { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public UserStatus Status { get; set; }
        public string Description { get; set; }
        public long CompanyId { get; set; }
        public long RoleId { get; set; }
        #endregion

        #region Calculated Properties
        public string FullName => $"{FirstName} {LastName}";

        #endregion

    }
}
