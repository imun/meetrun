﻿using System.ComponentModel.DataAnnotations;
using MeetRun.Core.Domain.Types;
using MeetRun.Web.Resources;
using MeetRun.Web.ViewModels.Base;

namespace MeetRun.Web.ViewModels.Entities {
    public class PlaceViewModel: BaseEntityViewModel {
        #region Properties
        public long Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_ProjectName")]
        [StringLength(300, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Title { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_ProjectName")]
        [StringLength(500, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Address { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_ProjectName")]
        [StringLength(500, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string GoogleMapCode { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_ProjectName")]
        [StringLength(500, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Description { get; set; }

        public long CompanyId { get; set; }
        public long UserId { get; set; }
        public EntityStatus Status { get; set; }
        #endregion

    }
}
