﻿using System.ComponentModel.DataAnnotations;
using Farasun.Library.Security;
using MeetRun.Web.Resources;
using MeetRun.Web.ViewModels.Base;

namespace MeetRun.Web.ViewModels.Account {
    public class LoginViewModel: BaseViewModel {
        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Username")]
        [StringLength(1000, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Password")]
        [StringLength(100, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Password { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_RememberMe")]
        public bool RememberMe { get; set; }

        public string PasswordHash => PasswordHasher.Md5Hash(Password);
    }
}
