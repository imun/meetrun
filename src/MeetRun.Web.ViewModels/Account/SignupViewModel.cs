﻿using System.ComponentModel.DataAnnotations;
using MeetRun.Web.Resources;
using MeetRun.Web.ViewModels.Base;

namespace MeetRun.Web.ViewModels.Account {
    public class SignupViewModel: BaseViewModel {
        public long UserId { get; set; }
        public long CompanyId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Email")]
        [StringLength(1000, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_FirstName")]
        [StringLength(300, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_LastName")]
        [StringLength(300, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Password")]
        [StringLength(50, MinimumLength = 8, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Password_Lenght")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_RetypePassword")]
        [StringLength(50, MinimumLength = 8, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Password_Lenght")]
        [DataType(DataType.Password)]
        public string RetypePassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Company_Name")]
        [StringLength(500, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string CompanyName { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_IsPersonal")]
        public bool IsPersonal { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Company_Email")]
        [StringLength(1000, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        [DataType(DataType.EmailAddress)]
        public string CompanyEmail { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Company_EnTitle")]
        [StringLength(500, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string CompanyEnTitle { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Company_Slug")]
        [StringLength(300, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Slug { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Mobile")]
        [StringLength(100, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        public string Mobile { get; set; }

        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_Website")]
        [StringLength(1000, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_ValueIsTooLong")]
        [DataType(DataType.Url)]
        public string Website { get; set; }
    }
}
