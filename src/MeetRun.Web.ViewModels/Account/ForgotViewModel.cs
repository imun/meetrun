﻿using MeetRun.Web.ViewModels.Base;
namespace MeetRun.Web.ViewModels.Account {
    public class ForgotViewModel: BaseViewModel {
        public string Username { get; set; }
    }
}
