﻿using System.ComponentModel.DataAnnotations;
using MeetRun.Web.ViewModels.Base;
using MeetRun.Web.Resources;

namespace MeetRun.Web.ViewModels.Account {
    public class SignupEnterCodeViewModel: BaseViewModel {
        public long UserId { get; set; }
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Required_SignupCode")]
        [Display(ResourceType = typeof(ViewModelStrings), Name = "Display_SignupCode")]
        [StringLength(6, MinimumLength = 6, ErrorMessageResourceType = typeof(ErrorStrings), ErrorMessageResourceName = "Error_Code_Lenght")]
        [DataType(DataType.Text)]
        public string Code { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
    }
}
