﻿using System.Collections.Generic;
namespace MeetRun.Web.ViewModels.Base {
    public class DropDownViewModel {
        public List<DropDownItemViewModel> items { get; set; }
        
        public DropDownViewModel() {
            items = new List<DropDownItemViewModel>();
        }
    }

    public class DropDownItemViewModel {
        public object id { get; set; }
        public string display { get; set; }
    }
}
