﻿using System;
using Farasun.Library.PersianTools.DateTime;
namespace MeetRun.Web.ViewModels.Base {
    public abstract class BaseEntityViewModel: BaseViewModel {
        public DateTime InsertDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string InsertDateDisplay => InsertDate.ToPersianDateString(PersianDateFormat.DayNum_MonthName_Year);
        public string ModifyDateDisplay => ModifyDate.ToPersianDateString(PersianDateFormat.DayNum_MonthName_Year);
    }
}
