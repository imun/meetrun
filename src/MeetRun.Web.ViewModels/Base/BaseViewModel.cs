﻿namespace MeetRun.Web.ViewModels.Base {
    public abstract class BaseViewModel {
        public bool HasError { get; set; }
        public bool Success { get; set; }
        public string ModelMessage { get; set; }
        public string ErrorFieldName { get; set; }
    }
}
