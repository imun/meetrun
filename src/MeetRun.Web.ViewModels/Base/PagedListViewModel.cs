﻿using System.Text;
using System.Collections.Generic;
namespace MeetRun.Web.ViewModels.Base {
    public class PagedListViewModel<T> where T: class {
        public PagedListViewModel() {
            PageSize = 10;
        }
        public string Title { get; set; }
        public List<T> Items { get; set; }
        public object ParentId { get; set; }
        public string ParentTitle { get; set; }
        public bool HasPagination { get; set; }
        public int PageSize { get; set; }
        public int TotalCount {
            get { return Items != null ? Items.Count : 0;  }
        }
        public int PageCount {
            get {
                if (TotalCount == 0) return 0;
                var pg_cnt = TotalCount / PageSize;
                var mod = TotalCount % PageSize;
                if (mod > 0)
                    pg_cnt++;
                return pg_cnt;
            }
        }
        public int PageNumber { get; set; }
        public int Counter { get; set; }
        public int StartIndex => ((PageNumber * PageSize) - PageSize) + 1;
        public int FinishIndex => (StartIndex + PageSize) - 1;
        public string OrderBy { get; set; }
        public string PaginationHtmlCode(string url) {
            var result = new StringBuilder();
            var format_pageItem = "<li class=\"page-item\"><a href=\"{0}\" class=\"page-link\">{1}</a></li>";
            var format_currPage = "<li class=\"page-item active\"><a href=\"#\" class=\"page-link\">{0}</a></li>";
            result.AppendLine("<ul class=\"pagination\">");
            for (int i = 0; i < PageCount; i++) {
                if (i == 0 && PageNumber == 1)
                    result.AppendLine(string.Format(format_currPage, i + 1));
                else if (i == 0 && PageNumber > 1) {
                    result.AppendLine(string.Format(format_pageItem,
                        url + $"?orderby={OrderBy}", i + 1));
                }
                else if (i == PageNumber - 1) {
                    result.AppendLine(string.Format(format_currPage, i + 1));
                }
                else {
                    result.AppendLine(string.Format(format_pageItem,
                        url + $"?page={i + 1}&orderby={OrderBy}", i + 1));
                }
            }
            result.AppendLine("</ul>");
            return result.ToString();
        }
        public string OrderDirection { get; set; }
        public string OrderByUrl(string url, string _property) {
            if (PageNumber == 1)
                return $"{url}?orderby={_property}";
            return $"{url}?page={PageNumber}&orderby={_property}";
        }
    }
}
