﻿using System;
namespace MeetRun.Core.Utils {
    public static class SignupCodeGenerator {
        public static string Generate(string email) {
            //var now = DateTime.Now;
            //var template = $"{now.Year}{now.Month}{now.Day}{now.Minute}{now.Second}{now.Millisecond}"
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[6];
            var random = new Random();
            for (int i = 0; i < stringChars.Length; i++) {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            var finalString = new String(stringChars);
            return finalString;
        }
    }
}
