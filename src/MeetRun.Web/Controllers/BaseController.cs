﻿using System.Web.Mvc;
using System.Web.Routing;
using MeetRun.Core.Services;
using MeetRun.Web.Codes.Security;

namespace MeetRun.Web.Controllers {
    public abstract class BaseController : Controller {
        protected SecurityControl _security { get; set; }
        protected DbContext Database { get; set; }
        protected override void Initialize(RequestContext requestContext) {
            base.Initialize(requestContext);
            _security = new SecurityControl(Session);
            if (Database == null) Database = new DbContext();
            if (_security.UserData != null)
                ViewBag.UserData = _security.UserData;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext) {
            if (Database == null) Database = new DbContext();
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext) {
            Database?.Dispose();
            base.OnActionExecuted(filterContext);
        }

        
    }
}