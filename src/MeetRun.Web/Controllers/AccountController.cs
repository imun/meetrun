﻿using System;
using System.Web.Mvc;
using MeetRun.Web.ViewModels.Account;
using MeetRun.Core.Domain;
using MeetRun.Core.Domain.Types;
using MeetRun.Core.Utils;
using MeetRun.Web.ViewModels.Entities;
using Nelibur.ObjectMapper;

namespace MeetRun.Web.Controllers {
    public class AccountController : BaseController {

        [HttpGet]
        public ActionResult Login() {
            var model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model) {
            if(!ModelState.IsValid) {
                model.HasError = true;
                model.ModelMessage = "لطفاً خطاهای فرم را بررسی کرده و دوباره فرم را ارسال نمایید.";
                return View(model);
            }
            var user = Database.Users.FirstOrDefault(x=> x.Email.ToLower() == model.Username.ToLower());
            if(user != null && user.Password == model.PasswordHash) {
                if(user.Status != UserStatus.Enabled) {
                    model.HasError = true;
                    model.ModelMessage = "حساب کاربری شما مسدود می باشد. در صورت اشتباه با ما تماس بگیرید.";
                    return View(model);
                }
                _security.Login(TinyMapper.Map<UserViewModel>(user));
                return RedirectToAction("Index", "Home", new { area = "App" });
            }
            model.HasError = true;
            model.ModelMessage = @"نام کاربری یا کلمه عبور وارد شده اشتباه است.";
            return View(model);
        } 

        [HttpGet]
        public ActionResult Signup() {
            var model = new SignupViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Signup(SignupViewModel model) {
            if(!ModelState.IsValid) {
                model.HasError = true;
                model.ModelMessage = "لطفاً خطاهای فرم را بررسی کرده و دوباره فرم را ارسال نمایید.";
                return View(model);
            }
            var user = TinyMapper.Map<User>(model);
            user.Email = user.Email.Trim();
            if (Database.Users.EmailExist(model.Email)) {
                model.HasError = true;
                ModelState.AddModelError("Email", "*");
                model.ModelMessage = $"آدرس ایمیل '{model.Email}' قبلاً در سیستم ثبت شده، اگر ایمیل شماست لطفاً با آن وارد شوید یا با ایمیل دیگری را امتحان کنید.";
                return View(model);
            }
            var role = Database.Roles.GetByNamespace("panel.manager");
            user.RoleId = role.Id;
            var company = new Company {
                Website = model.Website,
                Slug = model.Slug,
                IsPersonal = model.IsPersonal,
                Email = model.CompanyEmail,
                EnTitle = model.CompanyEnTitle,
                InsertDate = DateTime.Now,
                ModifyDate = DateTime.Now,
                Status = EntityStatus.Enabled,
                Title = model.CompanyName
            };
            Database.Companies.Insert(company);
            user.CompanyId = company.Id;
            var signupCode = SignupCodeGenerator.Generate(user.Email);
            while(Database.Users.SignupCodeExist(signupCode)) {
                signupCode = SignupCodeGenerator.Generate(user.Email);
            }
            user.SignupCode = signupCode;
            user.Status = UserStatus.Disabled;
            Database.Users.Insert(user);
            _security.SetTempUserId(user.Id);
            return RedirectToAction("Enter");
        }

        [HttpGet]
        public ActionResult Enter(string code) {
            if (!_security.TempUserId.HasValue  && string.IsNullOrEmpty(code))
                return RedirectToAction("Login", "Account");
            User user = null;
            if(_security.TempUserId.HasValue) {
                user = Database.Users.FirstOrDefault(x => x.Id == _security.TempUserId.Value);  
            }
            else if(!string.IsNullOrEmpty(code)) {
                user = Database.Users.GetBySignupCode(code);
            }
            else {
                return RedirectToAction("Login", "Account");
            }
            if (user == null)
                return RedirectToAction("Login", "Account");
            var model = new SignupEnterCodeViewModel {
                Email = user.Email,
                Code = code,
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Enter(SignupEnterCodeViewModel model) {
            if(!ModelState.IsValid) {
                model.HasError = true;
                model.ModelMessage = "";
                return View(model);
            }
            var user = Database.Users.FirstOrDefault(x=> x.Id == model.UserId);
            if(user == null) {
                model.HasError = true;
                return View(model);
            }
            if(user.SignupCode.ToLower() == model.Code.ToLower()) {
                var userModel = TinyMapper.Map<UserViewModel>(user);
                user.Status = UserStatus.Enabled;
                Database.Users.Update(user);
                _security.Login(userModel);
                return RedirectToAction("Index", "Home", new {area = "App"});
            }
            model.HasError = true;
            model.ModelMessage = "کد وارد شده نادرست است.";
            return View(model);
        }

        [HttpGet]
        public ActionResult Forgot() {
            var model = new ForgotViewModel();
            return View(model);
        }

        [HttpGet]
        public ActionResult Logout() {
            _security.Logout();
            return RedirectToAction("Login", "Account");
        }
    }
}