﻿using System.Web.Mvc;
namespace MeetRun.Web.Codes.Security {
    public class LoginControl: AuthorizeAttribute {
        public override void OnAuthorization(AuthorizationContext filterContext) {

            string actionName = filterContext.ActionDescriptor.ActionName.Trim().ToLower(),
                controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.Trim().ToLower();

            var helper = new UrlHelper(filterContext.RequestContext);
            var loginUrl = helper.Action("Login", "Account", new { area = "" });

            var security = new SecurityControl(filterContext.HttpContext.Session);
            if (security.LoggedIn) {
                //if ((UserType)security.UserData.UserType != UserType.Admin)
                //filterContext.Result = new RedirectResult(loginUrl
                //    + string.Format("?returnUrl={0}", filterContext.HttpContext.Request.Url));
            }
            else {
                filterContext.Result = new RedirectResult(loginUrl
                                                          + string.Format("?returnUrl={0}", filterContext.HttpContext.Request.Url));
            }
        }
    }
}