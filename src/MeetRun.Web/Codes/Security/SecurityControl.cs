﻿using System;
using System.Web;
using MeetRun.Web.ViewModels.Entities;
namespace MeetRun.Web.Codes.Security {
    /// <summary>
    /// Security control class for MeetRun
    /// </summary>
    public class SecurityControl {
        public SecurityControl(HttpSessionStateBase session) {
            _session = session;
        }

        #region Fields & Properties
        private HttpSessionStateBase _session;
        private UserViewModel _userData;
        public UserViewModel UserData {
            get {
                if (_session["user.data"] != null)
                    return (UserViewModel)_session["user.data"];
                return null;
            }
            set { _session["user.data"] = value; }
        }

        public long? TempUserId {
            get {
                if (_session["temp.user.id"] != null)
                    return Convert.ToInt64(_session["temp.user.id"].ToString());
                return null;
            }
            set { _session["temp.user.id"] = value; }
        }
        
        #endregion
        public void SetTempUserId(long id) {
            TempUserId = id;
        }

        public void ClearTempUserId() {
            TempUserId = null;
        }

        public bool LoggedIn {
            get { return UserData != null; }
        }
        public void Login(UserViewModel user) {
            UserData = user;
        }
        public void Logout() {
            UserData = null;
            _session.Abandon();
        }
    }
}