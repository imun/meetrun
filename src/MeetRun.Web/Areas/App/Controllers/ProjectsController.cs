using MeetRun.Web.Controllers;
using System;
using System.Linq;
using System.Web.Mvc;
using MeetRun.Web.ViewModels.Entities;
using MeetRun.Web.ViewModels.Base;
using MeetRun.Web.Codes.Security;
using MeetRun.Core.Domain;
using Nelibur.ObjectMapper;

namespace MeetRun.Web.Areas.App.Controllers {
    public class ProjectsController : BaseController {
        // GET: App/Projects
        [HttpGet]
        [LoginControl]
        public ActionResult Index() {
            var model = new PagedListViewModel<ProjectViewModel> {
                Items = Database.Projects.GetList(_security.UserData.CompanyId)
                    .Select(x => TinyMapper.Map<ProjectViewModel>(x)).ToList()
            };
            return View(model);
        }

        [HttpGet]
        [LoginControl]
        public ActionResult New() {
            var model = new ProjectViewModel();
            return View(model);
        }


        [HttpPost]
        [LoginControl]
        public ActionResult New(ProjectViewModel model) {
            if(!ModelState.IsValid) {
                model.HasError = true;
                return View(model);
            }
            var project = TinyMapper.Map<Project>(model);
            project.CompanyId = _security.UserData.CompanyId;
            project.InsertDate = project.ModifyDate = DateTime.Now;
            project.UserId = _security.UserData.Id;
            Database.Projects.Insert(project);
            return RedirectToAction("Index", "Projects");
        }

        [HttpGet]
        [LoginControl]
        public ActionResult Edit(long id) {
            var data = Database.Projects.Get(id);
            var model = TinyMapper.Map<ProjectViewModel>(data);
            return View(model);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult Edit(long id, ProjectViewModel model) {
            if(!ModelState.IsValid) {
                model.HasError = true;
                return View(model);
            }
            var project = TinyMapper.Map<Project>(model);
            project.ModifyDate = DateTime.Now;
            project.UserId = _security.UserData.Id;
            //Database.Projects.Update(project);
            model.Success = true;
            return View(model);
        }

        //[HttpPost]
        //[LoginControl]
        //public ActionResult Delete(long id) {
        //    var model = new Project() { Id = id };
        //    Database.Projects.Delete(model);
        //}
    }
}