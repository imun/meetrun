﻿using MeetRun.Web.Codes.Security;
using MeetRun.Web.Controllers;
using System.Web.Mvc;
using MeetRun.Web.ViewModels.Entities;
using MeetRun.Web.ViewModels.Base;

namespace MeetRun.Web.Areas.App.Controllers {
    public class PlacesController : BaseController {
        // GET: App/Places
        [HttpGet]
        [LoginControl]
        public ActionResult Index() {
            var model = new PagedListViewModel<PlaceViewModel>();
            return View(model);
        }

        [HttpGet]
        [LoginControl]
        public ActionResult New() {
            var model = new PlaceViewModel();
            return View(model);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult New(PlaceViewModel model) {
            return View(model);
        }

        [HttpGet]
        [LoginControl]
        public ActionResult Edit(long id) {
            var model = Database.Places.
            return View(model);
        }

        [HttpPost]
        [LoginControl]
        public ActionResult Edit(long id, PlaceViewModel model) {
            return View(model);
        }

    }
}