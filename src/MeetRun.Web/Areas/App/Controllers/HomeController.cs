﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetRun.Web.Codes.Security;

namespace MeetRun.Web.Areas.App.Controllers {
    public class HomeController : Controller {
        // GET: App/Home
        [HttpGet]
        [LoginControl]
        public ActionResult Index() {
            return View();
        }
    }
}