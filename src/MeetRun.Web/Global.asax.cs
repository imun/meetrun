﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MeetRun.Core.Services.Database;

namespace MeetRun.Web {
    public class MvcApplication : HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            DbFactory.Setup();
        }
    }
}
