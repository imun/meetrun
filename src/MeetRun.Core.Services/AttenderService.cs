﻿using Farasun.Library.Data;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class AttenderService: DataRepository<Attender> {
        public AttenderService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public AttenderService(IDatabase databaseConnection) : base(databaseConnection) {
        }
    }
}
