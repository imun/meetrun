﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Farasun.Library.Data;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class ProjectService: DataRepository<Project> {
        public ProjectService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public ProjectService(IDatabase databaseConnection) : base(databaseConnection) {
        }

        #region Queries
        private const string QueryGetList = @"SELECT * FROM [Projects] WHERE [CompanyId] = @0";

        #endregion

        public Project Get(long id) {
            return Connection.FirstOrDefault<Project>("SELECT * FROM [Projects] WHERE [Id] = @0", id);
        }

        public List<Project> GetList(long companyId) {
            return Connection.Query<Project>(QueryGetList, companyId).ToList();
        }
    }
}
