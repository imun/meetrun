﻿using Farasun.Library.Data.Server;
using NPoco;
using NPoco.FluentMappings;
namespace MeetRun.Core.Services.Database {
    public class DbFactory {
        public static DatabaseFactory Factory { get; set; }
        public static void Setup() {
            var fluentConfig = FluentMappingConfiguration.Configure();
            Factory = NPoco.DatabaseFactory.Config(x => {
                x.UsingDatabase(() => new NPoco.Database(DbServer.Open("SqlServer")));
                x.WithFluentConfig(fluentConfig);
            });
        }
    }
}
