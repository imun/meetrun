﻿using Farasun.Library.Data;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class MeetingService: DataRepository<Meeting> {
        public MeetingService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public MeetingService(IDatabase databaseConnection) : base(databaseConnection) {
        }
    }
}
