﻿using System;
using Farasun.Library.Data;
using Farasun.Library.PersianTools.PersianString;
using Farasun.Library.Security;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class UserService: DataRepository<User> {
        public UserService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public UserService(IDatabase databaseConnection) : base(databaseConnection) {
        }

        #region
        private const string TableName = @"Users";
        private const string Query_SignupCodeExist = @"SELECT COUNT(Id) FROM [Users] WHERE LOWER([SignupCode]) = @0";
        private const string Query_EmailExist = @"SELECT COUNT(Id) FROM [Users] WHERE LOWER([Email]) = @0";
        private const string Query_GetBySignupCode = @"SELECT * FROM [Users] WHERE LOWER([SignupCode]) = @0";

        #endregion

        #region Base Methods

        private void FixStringFields(ref User model) {
            if(!string.IsNullOrEmpty(model.FirstName)) model.FirstName = model.FirstName.Trim().FixYeKe();
            if(!string.IsNullOrEmpty(model.LastName)) model.LastName = model.LastName.Trim().FixYeKe();
            if(!string.IsNullOrEmpty(model.Website)) model.Website = model.Website.Trim();
            if(!string.IsNullOrEmpty(model.Description)) model.Description = model.Description.FixYeKe();
            if(!string.IsNullOrEmpty(model.Mobile)) model.Mobile = model.Mobile.Trim();
            if(!string.IsNullOrEmpty(model.Password)) model.Password = PasswordHasher.Md5Hash(model.Password);
            if(!string.IsNullOrEmpty(model.Phone)) model.Phone = model.Phone.Trim();
            if(!string.IsNullOrEmpty(model.Email)) model.Email = model.Email.Trim();
            if (!string.IsNullOrEmpty(model.TelegramId)) model.TelegramId = model.TelegramId.Trim();
        }

        public new void Insert(User model) {
            FixStringFields(ref model);
            model.InsertDate = model.ModifyDate = DateTime.Now;
            base.Insert(model);
        }

        public new void Update(User model) {
            FixStringFields(ref model);
            model.ModifyDate = DateTime.Now;
            base.Update(model);
        }


        #endregion

        #region Methods

        public User GetBySignupCode(string code) {
            return Connection.FirstOrDefault<User>(Query_GetBySignupCode, code.ToLower());
        }

        public bool SignupCodeExist(string code) {
            return Connection.ExecuteScalar<int>(Query_SignupCodeExist, code.ToLower()) > 0;
        }

        public bool EmailExist(string email) {
            return Connection.ExecuteScalar<int>(Query_EmailExist, email.ToLower()) > 0;
        }
        #endregion

    }
}
