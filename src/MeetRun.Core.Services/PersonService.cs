﻿using Farasun.Library.Data;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class PersonService: DataRepository<Person> {
        public PersonService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public PersonService(IDatabase databaseConnection) : base(databaseConnection) {
        }
    }
}
