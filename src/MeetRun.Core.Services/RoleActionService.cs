﻿using Farasun.Library.Data;
using MeetRun.Core.Domain.Security;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class RoleActionService: DataRepository<RoleAction> {
        public RoleActionService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public RoleActionService(IDatabase databaseConnection) : base(databaseConnection) {
        }
    }
}
