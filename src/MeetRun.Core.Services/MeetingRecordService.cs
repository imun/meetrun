﻿using Farasun.Library.Data;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class MeetingRecordService: DataRepository<MeetingRecord> {
        public MeetingRecordService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public MeetingRecordService(IDatabase databaseConnection) : base(databaseConnection) {
        }
    }
}
