﻿using Farasun.Library.Data;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class AttachmentService: DataRepository<Attachment> {
        public AttachmentService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public AttachmentService(IDatabase databaseConnection) : base(databaseConnection) {
        }
    }
}
