﻿using Farasun.Library.Data;
using MeetRun.Core.Domain.Security;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class UserLoginService: DataRepository<UserLogin> {
        public UserLoginService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public UserLoginService(IDatabase databaseConnection) : base(databaseConnection) {
        }
    }
}
