﻿using Farasun.Library.Data;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class PlaceService: DataRepository<Place> {
        public PlaceService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public PlaceService(IDatabase databaseConnection) : base(databaseConnection) {
        }

        
    }
}
