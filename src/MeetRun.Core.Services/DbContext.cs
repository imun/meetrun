﻿using System;
using System.Data;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class DbContext: IDisposable {
        public DbContext() {
            Connection = DbFactory.Factory.GetDatabase();
            if (Connection.Connection.State != ConnectionState.Open)
                Connection.Connection.Open();
            Actions = new ActionService(Connection);
            Attachments = new AttachmentService(Connection);
            Attenders = new AttenderService(Connection);
            Companies  = new CompanyService(Connection);
            MeetingRecords = new MeetingRecordService(Connection);
            Meetings = new MeetingService(Connection);
            Persons = new PersonService(Connection);
            Places = new PlaceService(Connection);
            Projects = new ProjectService(Connection);
            RoleActions = new RoleActionService(Connection);
            Roles = new RoleService(Connection);
            UserLogins = new UserLoginService(Connection);
            Users = new UserService(Connection);
        }
        #region Properties

        private IDatabase _connection;
        public IDatabase Connection {
            get { return _connection; }
            set { _connection = value; }
        }

        public ActionService Actions { get; set; }
        public AttachmentService Attachments { get; set; }
        public AttenderService Attenders { get; set; }
        public CompanyService Companies { get; set; }
        public MeetingRecordService MeetingRecords { get; set; }
        public MeetingService Meetings { get; set; }
        public PersonService Persons { get; set; }
        public PlaceService Places { get; set; }
        public ProjectService Projects { get; set; }
        public RoleActionService RoleActions { get; set; }
        public RoleService Roles { get; set; }
        public UserLoginService UserLogins { get; set; }
        public UserService Users { get; set; }

        #endregion

        public void Dispose() {
            if (_connection.Connection.State == ConnectionState.Open)
                _connection.Connection.Close();
            _connection?.Dispose();
        }
    }
}
