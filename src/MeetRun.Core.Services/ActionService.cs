﻿using Farasun.Library.Data;
using MeetRun.Core.Services.Database;
using NPoco;
using Action = MeetRun.Core.Domain.Security.Action;

namespace MeetRun.Core.Services {
    public class ActionService: DataRepository<Action> {
        public ActionService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public ActionService(IDatabase databaseConnection) : base(databaseConnection) {
        }
    }
}
