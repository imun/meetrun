﻿using Farasun.Library.Data;
using MeetRun.Core.Domain.Security;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class RoleService: DataRepository<Role> {
        public RoleService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public RoleService(IDatabase databaseConnection) : base(databaseConnection) {
        }

        #region Queries

        private const string TableName = "Security_Roles";
        private const string Query_GetRoleyByNamespace = @"SELECT * FROM [Security_Roles] WHERE LOWER([Namespace]) = @0";


        #endregion

        #region Methods
        public Role GetByNamespace(string _namespace) {
            //return FirstOrDefault(x => x.Namespace.ToLower() == _namespace.ToLower());
            return Connection.FirstOrDefault<Role>(Query_GetRoleyByNamespace, _namespace.ToLower());
        }

        #endregion

    }
}
