﻿using System;
using Farasun.Library.Data;
using Farasun.Library.PersianTools.PersianString;
using MeetRun.Core.Domain;
using MeetRun.Core.Services.Database;
using NPoco;

namespace MeetRun.Core.Services {
    public class CompanyService: DataRepository<Company> {
        public CompanyService() {
            Connection = DbFactory.Factory.GetDatabase();
        }
        public CompanyService(IDatabase databaseConnection) : base(databaseConnection) {
        }

        #region Main Methods

        private void FixStringFields(ref Company model) {
            if(!string.IsNullOrEmpty(model.Website)) model.Website = model.Website.Trim();
            if(!string.IsNullOrEmpty(model.Address)) model.Address = model.Address.Trim().FixYeKe();
            if(!string.IsNullOrEmpty(model.CeoName)) model.CeoName = model.CeoName.Trim().FixYeKe();
            if(!string.IsNullOrEmpty(model.CeoPhone)) model.CeoPhone = model.CeoPhone.Trim().FixYeKe();
            if(!string.IsNullOrEmpty(model.Description)) model.Description = model.Description.FixYeKe();
            if(!string.IsNullOrEmpty(model.Email)) model.Email = model.Email.Trim();
            if(!string.IsNullOrEmpty(model.EconomyId)) model.EconomyId = model.EconomyId.Trim();
            if(!string.IsNullOrEmpty(model.EnTitle)) model.EnTitle = model.EnTitle.Trim();
            if(!string.IsNullOrEmpty(model.Fax)) model.Fax = model.Fax.Trim();
            if(!string.IsNullOrEmpty(model.Phone)) model.Phone = model.Phone.Trim();
            if(!string.IsNullOrEmpty(model.Slug)) model.Slug = model.Slug.Trim().FixYeKe();
            if(!string.IsNullOrEmpty(model.RegisterNumber)) model.RegisterNumber = model.RegisterNumber.Trim();
            if(!string.IsNullOrEmpty(model.Title)) model.Title = model.Title.Trim().FixYeKe();
        }

        public new void Insert(Company model) {
            FixStringFields(ref model);
            model.InsertDate = model.ModifyDate = DateTime.Now;
            base.Insert(model);
        }

        public new void Update(Company model) {
            FixStringFields(ref model);
            model.ModifyDate = DateTime.Now;
            base.Update(model);
        }

        #endregion
    }
}
