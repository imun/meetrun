﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetRun.Core.Domain.Types {
    public enum UserStatus {
        Deleted = -1,
        Disabled = 0,
        Enabled = 1,
        Banned = 2
    }
}
