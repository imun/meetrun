﻿namespace MeetRun.Core.Domain.Types {
    public enum AttenderStatus {
        Absent = -1,
        Canceled = 0,
        Present = 1
    }
}
