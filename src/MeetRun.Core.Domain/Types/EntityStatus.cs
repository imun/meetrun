﻿namespace MeetRun.Core.Domain.Types {
    public enum EntityStatus {
        Deleted = -1,
        Disabled = 0,
        Enabled = 1
    }
}
