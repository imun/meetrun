﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetRun.Core.Domain.Types {
    public enum MeetingStatus {
        Deleted = -1,
        Stopped = 0,
        Running = 1,
        Canceled = 2
    }
}
