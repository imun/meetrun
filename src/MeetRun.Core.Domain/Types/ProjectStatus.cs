﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetRun.Core.Domain.Types {
    public enum ProjectStatus {
        Deleted = -1,
        Disabled = 0,
        Enabled = 1,
        Started = 2,
        InProgress = 3,
        Completed = 4,
        Failed = 5
    }
}
