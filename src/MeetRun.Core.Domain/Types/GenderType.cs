﻿namespace MeetRun.Core.Domain.Types {
    public enum GenderType {
        Unknown = -1,
        Female = 0,
        Male = 1
    }
}
