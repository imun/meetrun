﻿using System;
using MeetRun.Core.Domain.Base;
using MeetRun.Core.Domain.Types;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Attenders")]
    public class Attender: BaseEntity {
        public long PersonId { get; set; }
        public long MeetingId { get; set; }
        public DateTime? PresentTime { get; set; }
        public AttenderStatus Status { get; set; }
    }
}
