﻿using MeetRun.Core.Domain.Base;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Meeting_Records")]
    public class MeetingRecord: BaseEntity {
        public long Id { get; set; }
        public long MeetingId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int OrderNumber { get; set; }
        public long UserId { get; set; }
    }
}

