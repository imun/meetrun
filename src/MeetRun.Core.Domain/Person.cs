﻿using System;
using MeetRun.Core.Domain.Base;
using MeetRun.Core.Domain.Types;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Persons")]
    public class Person: BaseEntity {
        public long Id { get; set; }
        public string InitName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public GenderType Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Description { get; set; }
        public EntityStatus Status { get; set; }
        public long CompanyId { get; set; }
        public long? UserId { get; set; }
    }
}
