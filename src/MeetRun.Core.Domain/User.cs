﻿using MeetRun.Core.Domain.Base;
using MeetRun.Core.Domain.Types;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Users")]
    public class User: BaseEntity {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string TelegramId { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public UserStatus Status { get; set; }
        public string SignupCode { get; set; }
        public string Description { get; set; }
        public long CompanyId { get; set; }
        public long RoleId { get; set; }
        public long? EditUserId { get; set; }

    }
}
