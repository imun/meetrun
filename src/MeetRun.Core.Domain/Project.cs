﻿using MeetRun.Core.Domain.Base;
using MeetRun.Core.Domain.Types;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Projects")]
    public class Project: BaseEntity {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string LogoPath { get; set; }
        public ProjectStatus Status { get; set; }
        public long UserId { get; set; }
        public long CompanyId { get; set; }
    }
}
