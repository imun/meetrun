﻿using MeetRun.Core.Domain.Base;
using MeetRun.Core.Domain.Types;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Places")]
    public class Place: BaseEntity {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string GoogleMapCode { get; set; }
        public string Description { get; set; }
        public long CompanyId { get; set; }
        public long UserId { get; set; }
        public EntityStatus Status { get; set; }
        
    }
}
