﻿using System;
using MeetRun.Core.Domain.Base;
using MeetRun.Core.Domain.Types;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Meetings")]
    public class Meeting: BaseEntity {
        public long Id { get; set; }
        public string Title { get; set; }
        public long CompanyId { get; set; }
        public long? ProjectId { get; set; }
        public long PlaceId { get; set; }
        public long AgentId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public bool FullDay { get; set; }
        public bool Repeatable { get; set; }
        public string Description { get; set; }
        public string Announcement { get; set; }
        public long UserId { get; set; }
        public MeetingStatus Status { get; set; }
    }
}
