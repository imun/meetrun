﻿using MeetRun.Core.Domain.Base;
using MeetRun.Core.Domain.Types;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Companies")]
    public class Company: BaseEntity {
        public long Id { get; set; }
        public string Title { get; set; }
        public string EnTitle { get; set; }
        public string EconomyId { get; set; }
        public string RegisterNumber { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string CeoName { get; set; }
        public string CeoPhone { get; set; }
        public string LogoPath { get; set; }
        public bool IsPersonal { get; set; }
        public EntityStatus Status { get; set; }
        public long? CreateUserId { get; set; }
    }
}
