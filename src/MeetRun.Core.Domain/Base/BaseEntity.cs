﻿using System;
namespace MeetRun.Core.Domain.Base {
    public interface IBaseEntity {

    }

    public abstract class BaseEntity: IBaseEntity {
        public DateTime InsertDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
