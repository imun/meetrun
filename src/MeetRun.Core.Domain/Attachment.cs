﻿using MeetRun.Core.Domain.Base;
using MeetRun.Core.Domain.Types;
using NPoco;
namespace MeetRun.Core.Domain {
    [TableName("Attachments")]
    public class Attachment: BaseEntity {
        public long Id { get; set; }
        public long MeetingId { get; set; }
        public long? RecordId { get; set; }
        public string Title { get; set; }
        public string Filename { get; set; }
        public string FileType { get; set; }
        public long UserId { get; set; }
        public EntityStatus Status { get; set; }
    }
}
