﻿using NPoco;
namespace MeetRun.Core.Domain.Security {
    [TableName("Security_Role_Actions")]
    public class RoleAction {
        public long RoleId { get; set; }
        public long ActionId { get; set; }
    }
}
