﻿using NPoco;
namespace MeetRun.Core.Domain.Security {
    [TableName("Security_Actions")]
    public class Action {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Controller { get; set; }
        public string ActionName { get; set; }
        public string Url { get; set; }
        public string RouteParams { get; set; }
    }
}
