﻿using System;
using NPoco;
namespace MeetRun.Core.Domain.Security {
    [TableName("Security_User_Logins")]
    public class UserLogin {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long CompanyId { get; set; }
        public string SessionActivityId { get; set; }
        public DateTime InsertDate { get; set; }
        public string UrlReferrer { get; set; }
        public string Ip { get; set; }
        public string UserAgent { get; set; }
        public string OsPlatform { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
    }
}
