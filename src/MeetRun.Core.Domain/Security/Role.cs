﻿using MeetRun.Core.Domain.Base;
using NPoco;
namespace MeetRun.Core.Domain.Security {
    [TableName("Security_Roles")]
    public class Role {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Namespace { get; set; }
        public string Description { get; set; }
    }
}
